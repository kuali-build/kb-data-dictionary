# Standard libraries
import sys
import os
import json
from json import JSONDecodeError

# Third-party libraries
import asyncio
import httpx
import uvloop
from loguru import logger

# Local application imports
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)
from data_structures import KualiQueries, Kuali


log_file = os.path.join(current_dir, 'kb-data-dictionary.log')
logger.add(log_file,
           format="{time:YYYY-MM-DD HH:mm:ssZZZ} {level} {message}",
           level="ERROR", enqueue=True)


async def get_response_data(url, headers, data, retries=3, timeout=60.0):
    async with httpx.AsyncClient(timeout=timeout, http2=True) as client:
        for retry in range(retries):
            try:
                response = await client.post(url, headers=headers, json=data)
                try:
                    response_json = response.json()
                    return response_json
                except JSONDecodeError:
                    logger.error(f"Error decoding JSON:\n{response.text}")
                    raise
            except httpx.ReadTimeout:
                if retry == retries - 1:
                    raise
                else:
                    logger.error(
                        f"Request timed out. Retrying ({retry + 1}/{retries})...")


async def main():
    url = Kuali.url()
    headers = Kuali.headers()
    query_apps = KualiQueries.AppIDs()
    data_apps = {"query": query_apps}
    response_json_apps = await get_response_data(url, headers, data_apps)
    returned_data = sorted(
        [{
            "id": app["id"], 
            "name": " ".join(app['name'].strip().split()),
            "schoolId": app['createdBy'].get('schoolId', 'N/A')
        } for app in response_json_apps["data"]["apps"]],
        key=lambda app: app['name'])

    for item in returned_data:
        item['name'] = ' '.join(item['name'].split())

    file_path = os.path.join(current_dir, 'apps.json')
    logger.info(file_path)

    with open(file_path, 'w') as f:
        json.dump(returned_data, f, indent=2)

    logger.info(f"App data saved to {file_path}")



if __name__ == "__main__":
    uvloop.install()
    asyncio.run(main())
