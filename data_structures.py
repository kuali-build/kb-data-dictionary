class JmespathQueries:
    @staticmethod
    def main_template(f, i):
        query = f"""
data.app.formContainer.template.children[{f}]{''.join(['.children[]']*i)}.{{
    "Custom json key": customFormKey.value, 
    "Kuali json key": formKey,
    "Gadget": type,
    "Possible iterations": details.allowAdditionalRows.maxRows || details.defaultRowCount || `1`,
    "Question": label,
    "Description": description.value,
    "Required": required,
    "Office Use": details.officeUseOnly,
    "Field ID": id,
    "Option label": details.options[*].lbl | [?@ != null] | [join(`; `, @)] | [0],
    "Option key": details.options[*].key | [?@ != null] | [join(`; `, @)] | [0],
    "Placeholder": details.placeholder.value,
    "Custom output field": details.selectedOutputField.label,
    "Custom input field - question": details.inputFields.question.value,
    "Custom input field - question_id": details.inputFields.question_id.value,
    "Display if": conditionalVisibility.value.type,
    "Display logic": conditionalVisibility.value.parts[*]
        }}
        """
        return query


    @staticmethod
    def children_template(f, i):
        query = f"""
data.app.formContainer.template.children[{f}]{''.join(['.children[]']*i)}.childrenTemplate[].{{
    "Custom json key": customFormKey.value, 
    "Kuali json key": formKey,
    "Gadget": type,
    "Possible iterations": details.allowAdditionalRows.maxRows || details.defaultRowCount || `1`,
    "Question": label,
    "Description": description.value,
    "Required": required,
    "Office Use": details.officeUseOnly,
    "Field ID": id,
    "Option label": details.options[*].lbl | [?@ != null] | [join(`; `, @)] | [0],
    "Option key": details.options[*].key | [?@ != null] | [join(`; `, @)] | [0],
    "Placeholder": details.placeholder.value,
    "Custom output field": details.selectedOutputField.label,
    "Display if": conditionalVisibility.value.type,
    "Display logic": conditionalVisibility.value.parts[*]
}}
        """
        return query

    @staticmethod
    def workflow_template():
        query = """
data.app.workflow.steps[].{
    "Step name": stepName,
    "Step Id": _id,
    "Step type": type,
    "Step assignee": assignee.type,
    "Step assignee form key": assignee.value.formKey,
    "Step assignee label": assignee.value.label,
    "Email subject": subject,
    "Email body": body,
    "Custom notification subject": customNotification.subject,
    "Custom notification body": customNotification.body,
    "Custom notification enabled": customNotification.enabled,
    "If denied action": ifDenied.action,
    "If denied then": ifDenied.andThen,
    "Reminder Frequency": join(' ', [to_string(reminderSettings.value.frequency), to_string(reminderSettings.value.frequencyType)]),
    "subflow name": subflows[].steps[].stepName | [?@ != null] | [join(`; `, @)] | [0],
    "subflow Id": subflows[].steps[]._id | [?@ != null] | [join(`; `, @)] | [0],
    "subflow type": subflows[].steps[].type | [?@ != null] | [join(`; `, @)] | [0],
    "subflow assignee": subflows[].steps[].assignee.type | [?@ != null] | [join(`; `, @)] | [0],
    "subflow assignee form key": subflows[].steps[].assignee.value.formKey | [?@ != null] | [join(`; `, @)] | [0],
    "subflow assignee label": subflows[].steps[].assignee.value.label | [?@ != null] | [join(`; `, @)] | [0],
    "subflow Email subject": subflows[].steps[].subject | [?@ != null] | [join(`; `, @)] | [0],
    "subflow Email body": subflows[].steps[].body | [?@ != null] | [join(`; `, @)] | [0],
    "subflow Custom notification subject": subflows[].steps[].customNotification.subject | [?@ != null] | [join(`; `, @)] | [0],
    "subflow Custom notification body": subflows[].steps[].customNotification.body | [?@ != null] | [join(`; `, @)] | [0],
    "subflow if denied action": subflows[].steps[].ifDenied.action | [?@ != null] | [join(`; `, @)] | [0],
    "subflow if denied then": subflows[].steps[].ifDenied.andThen | [?@ != null] | [join(`; `, @)] | [0],
    "subflow reminder frequency": join(' ', [to_string(subflows[].steps[].reminderSettings.value.frequency), to_string(subflows[].steps[].reminderSettings.value.frequencyType)])
}
"""
        return query

    @staticmethod
    def KualiStats():
        query = """
{
"Spaces": data.spaces[].{
    "ID": id,
    "name": name,
    "Integrations": integrationCount
},
"Apps": data.apps[].{
    "App ID": id,
    "App name": name,
    "created by name": createdBy.name,
    "created by duid": createdBy.schoolId,
    "created on date": createdAt,
    "Updated last": updatedAt,
    "space name": space.name,
    "space ID": space.id
},
"Integrations": data.integrationsConnection.edges[].{
    "Name": node.data.__name,
    "Decription": node.data.__description,
    "Type": node.data.__type.label,
    "Method": node.data.__method.label,
    "URL": node.data.__url,
    "Authentication": node.data.__authenticationType.label,
    "ID Key": node.data.__idKey,
    "Label Key": node.data.__labelKey,
    "Inputs": node.data.__queryParameterInputs[].__label | [?@ != null] | [join(`; `, @)] | [0],
    "Outputs": node.data.__outputs[].__label | [?@ != null] | [join(`; `, @)] | [0],
    "Output Gadgets": node.data.__outputs[].__gadget.type.label | [?@ != null] | [join(`; `, @)] | [0],
    "Connected Apps": node.appsUsing[].name | [?@ != null] | [join(`; `, @)] | [0],
    "Submissions": node.appsUsing[].dataset.documentConnection.totalCount | [?@ != null] | [*],
    "App Owners": node.appsUsing[].createdBy.displayName | [?@ != null] | [join(`; `, @)] | [0]
},
"Groups": data.groupsConnection.edges[].{
    "Group Name": node.name, 
    "Group ID": node.id,
    "Group category": node.category.name,
    "Group role schemas": node.roleSchemas[].name | [?@ != null] | [join(`; `, @)] | [0],
    "Group role schema descriptions":node.roleSchemas[].description | [?@ != null] | [join(`; `, @)] | [0],
    "Group role names":node.roles[].name | [?@ != null] | [join(`; `, @)] | [0],
    "Group role member names":node.roles[].members[].displayName | [?@ != null] | [join(`; `, @)] | [0],
    "Group role member ids":node.roles[].members[].schoolId | [?@ != null] | [join(`; `, @)] | [0]
}
}
"""
        return query


class Kuali:
    @staticmethod
    def url():
        return "https://duke.kualibuild.com/app/api/v0/graphql"

    @staticmethod
    def headers():
        import os
        from dotenv import load_dotenv
        load_dotenv()
        return {
            "Accept-Encoding": "gzip, deflate, br",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": "https://duke.kualibuild.com",
            "Authorization": f'{os.getenv("AUTH_TOKEN")}',
        }


class KualiQueries:
    @staticmethod
    def DataDictionary():
        return """
query($id: ID) {
app(id: $id) {
name
id
formContainer {
template
}
workflow
}
}
        """

    @staticmethod
    def SpaceIDs():
        return """
query KualiSpaceIDs {
spaces {
id
}
}
        """

    @staticmethod
    def AppIDs():
        return """
query apps {
apps {
name
id
createdBy {
schoolId
}
}
}
        """

    @staticmethod
    def construct_query_with_aliases(event_types):
        query = "query SystemAuditLogs($q: String) {"
        
        for event_type in event_types:
            alias = event_type.replace(" ", "_")
            query += f"""
            {alias}: auditLogsConnection(
                args: {{
                    q: $q
                    skip: 0
                    limit: 1
                    sort: ["-timestamp"]
                        filter: {{
                            eventType: ["{event_type}"]
                            contextualComponents: ["622f563dd68e3e042e8b5746"]
                        }}
                    }}
                ) {{
                totalCount
                edges {{
                    node {{
                        componentId
                        user {{
                            displayName
                            email
                        }}
                    timestamp
                    }}
                }}
            }}
            """
        
        query += "}"
        return query

    @staticmethod
    def construct_totals_by_day_query(event_types: set, days: int = 30):
        query = "query SystemAuditLogs($q: String) {"

        today = datetime.utcnow()
        for day in range(days):
            start_date = today - timedelta(days=day+1)
            end_date = today - timedelta(days=day)

            start_timestamp = int(start_date.timestamp() * 1000)
            end_timestamp = int(end_date.timestamp() * 1000)
            formatted_date = start_date.strftime("%B_%d_%Y")
            alias = f"{formatted_date}"
            query += f"""
            {alias}: auditLogsConnection(
                args: {{
                    q: $q
                    skip: 0
                    limit: 25
                    sort: []
                    filter: {{
                        eventType: ["DOCUMENT_SUBMIT"],
                        contextualComponents: ["622f563dd68e3e042e8b5746"]
                        timestamp: {{
                        min: {start_timestamp},
                        max: {end_timestamp}
                        }}
                    }}
                }}
            ) {{
                totalCount
            }}
            """

        query += "}"
        return query

    @staticmethod
    def InfoAndGroups():
        return """
query KualiInfoAndGroups($spaceId: ID!, $q: String, $categoryId: ID) {
spaces {
name
id
integrationCount
}
apps {
name
id
createdBy {
name
schoolId
}
createdAt
updatedAt
space {
name
id
}
}
integrationsConnection(spaceId: $spaceId) {
edges {
node {
data
appsUsing {
name
id
createdAt
updatedAt
createdBy {
displayName
schoolId
}
}
}
}
}
listPolicyGroups {
id
name
policies {
version
statements {
action
effect
resource
}
}
identities {
label
id
type
email
}
}
groupsConnection: groupsConnection(
args: {query: $q, skip: 0, limit: 100, sort: [], categoryId: $categoryId}
) {
totalCount
pageInfo {
hasNextPage
hasPreviousPage
skip
limit
}
edges {
node {
id
newId
name
parentId
category {
id
name
}
roleSchemas {
id
name
description
}
roles {
id
name
}
}
}
}
}
"""
