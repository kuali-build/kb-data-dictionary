
import sys
import os
import pwd
import re
import json
from datetime import datetime, timedelta
from enum import Enum, auto
from subprocess import call
import base64
import time
from typing import Optional, Annotated, List, Dict

# Third-party imports
import asyncio
import aiofiles
import httpx
import uvloop
import httptools
import nltk
import orjson
from dotenv import load_dotenv
from fastapi import (
    FastAPI,
    BackgroundTasks,
    Depends,
    HTTPException,
    Header,
    Query,
    Request,
    Security,
    status,
    File,
    UploadFile,
    Body
)
from fastapi.exceptions import RequestValidationError
from fastapi.middleware import Middleware
from fastapi.responses import (
    JSONResponse,
    ORJSONResponse,
    PlainTextResponse,
    HTMLResponse,
    FileResponse,
    StreamingResponse
)
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from loguru import logger
from pydantic import BaseModel
from pytz import timezone

# Local application imports
from get_apps import main as run_get_apps
from list_apps import KualiAppsEnum, app_name_to_id
from excel_builder import main_dictionary
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

security = HTTPBasic()

load_dotenv()


def verify_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = f"{os.getenv('BASIC_AUTH_USERNAME')}"
    correct_password = f"{os.getenv('BASIC_AUTH_PASSWORD')}"
    if credentials.username != correct_username or credentials.password != correct_password:
        raise HTTPException(
            status_code=401, detail="Incorrect username or password")
    return credentials


app = FastAPI(
    openapi_url="/openapi.json",
    docs_url="/docs",
    redoc_url="/redocs"
)


async def download_nltk_data():
    nltk.download('punkt')
    nltk.download('stopwords')


async def update_apps_json():
    try:
        await run_get_apps()
    except Exception as e:
        logger.error(f"Failed to update apps.json: {str(e)}")

@app.on_event("startup")
async def startup_event():
    uid = os.getuid()
    username = pwd.getpwuid(uid).pw_name
    logger.info(f"User is {username}")

    # Running these tasks in the background upon startup
    asyncio.create_task(update_apps_json())
    asyncio.create_task(download_nltk_data())

    logger.info("Application Started")


@app.on_event("shutdown")
async def shutdown_event():
    logger.info("Goodbye cruel world!")


@app.exception_handler(HTTPException)
async def http_exception_handler(request: Request, exc: HTTPException):
    return JSONResponse(status_code=exc.status_code, content={"detail": exc.detail})


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(timedelta(seconds=process_time))
    response.headers["X-HTTP-Protocol"] = request.scope.get("http_version")
    return response


class OutputOptionEnum(str, Enum):
    download = "download"
    upload = "upload"

upload_endpoint = os.getenv("UPLOAD_ENDPOINT")

@app.get("/apps", dependencies=[Depends(verify_credentials)])
async def get_apps(background_tasks: BackgroundTasks, school_id: Optional[str] = None):
    background_tasks.add_task(update_apps_json)
    try:
        with open(os.path.join(current_dir, 'apps.json'), "r") as read_file:
            apps = json.load(read_file)
        if school_id:
            apps = [app for app in apps if app.get('schoolId') == school_id]
        return ORJSONResponse(apps)
    except Exception as e:
        logger.error(f"Failed to load apps.json: {str(e)}")
        raise HTTPException(status_code=500, detail="Failed to load apps.json")


@app.post("/data_dictionary", dependencies=[Depends(verify_credentials)])
async def data_dictionaries(user: str, email: str, app_id: str, duid: str,
                            output_option: OutputOptionEnum = Query(OutputOptionEnum.download, include_in_schema=True)):
    filename = None
    try:
        App_Name = app_id
        file_object, filename = await main_dictionary(app_id)
        logger.info(f"User: {user} | requested file: {filename} | Date: {datetime.now(tz=timezone('US/Eastern')).strftime('%B %d, %Y %H:%M:%S')} - SUCCESS")

        if output_option == OutputOptionEnum.upload:
            App_Name = re.sub(r'[^\w\s]', '', App_Name)
            async with httpx.AsyncClient(http2=True, timeout=60.0) as client:
                data = {"Email": email,"App_Name": f'{App_Name}.xlsx', "Duke_id": duid}
                files = {"file": (filename, file_object, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
                        'document_data': (None, json.dumps(data).encode(), 'application/json')}
                response = await client.post(upload_endpoint, files=files)

            if response.status_code != 200:
                raise Exception(f"Failed to upload the file, status code: {response.status_code}")
            api_result = response.json()["shareLink"]
            response_from_api = f"<a href='{api_result}' target='_blank'>{filename}</a>"

            return {"response": response_from_api}

        elif output_option == OutputOptionEnum.download:
            return StreamingResponse(file_object, media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", headers={'Content-Disposition': f'attachment;filename={filename}'})

    except Exception as e:
        logger.error(f"User: {user} | requested file: {filename if filename else 'unknown'} | Date: {datetime.now(tz=timezone('US/Eastern')).strftime('%B %d, %Y %H:%M:%S')} - FAILURE: {str(e)}")
        raise e


if __name__ == "__main__":
    import uvicorn
    uvicorn.run(
        app=__name__ + ":app",
        host="0.0.0.0",
        port=int(os.environ.get("PORT", 8101)),
        proxy_headers=True,
        reload=False,
        root_path="/kbdatadictionary",
        loop="uvloop",
        http="httptools",
        workers=4,
        log_level="info",
        timeout_keep_alive=int(os.environ.get("KEEP_ALIVE", 60)),
        limit_max_requests=int(os.environ.get("MAX_REQUESTS", 1000)),
    )
