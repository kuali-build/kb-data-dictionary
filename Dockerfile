FROM python:slim

ENV DEBIAN_FRONTEND noninteractive
ENV ACCEPT_EULA=Y

WORKDIR /app

COPY requirements.txt .
COPY apps.json .
COPY *.py .

# Update and upgrade system packages, install necessary packages, clean up
RUN apt-get update && apt-get -yq dist-upgrade \
    && apt-get install -yq --no-install-recommends wget bzip2 build-essential software-properties-common vim unzip python3 libssl-dev libffi-dev python3-dev tdsodbc g++ apt-transport-https \
    && apt-get update \
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --upgrade pip \
    && pip install -r requirements.txt

EXPOSE 8101

CMD ["uvicorn", "endpoints:app", "--port", "8101", "--host", "0.0.0.0", "--root-path", "/kbdatadictionary", "--workers", "1", "--limit-concurrency", "100", "--timeout-keep-alive", "120", "--loop", "uvloop", "--log-level", "info", "--proxy-headers", "--http", "httptools"]
