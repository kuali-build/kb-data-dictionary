# Path to file: /home/sjc98/kb_dictionary/excel_builder.py

# Standard library imports
from data_structures import JmespathQueries, KualiQueries, Kuali
from excel_formatter import ExcelFormatter
import sys
import os
import asyncio
import contextlib
import re
import math
import time
import json
from io import BytesIO

# Third-party library imports
import httpx
import pandas as pd
import uvloop
from jmespath import search as jsearch
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
import traceback
from loguru import logger

# Used for shortening questions to 4 words or less
import nltk
from nltk.corpus import stopwords
from nltk.probability import FreqDist
from nltk.tokenize import word_tokenize


# Local application imports
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)


formatter = ExcelFormatter()
url = Kuali.url()
headers = Kuali.headers()
query = KualiQueries.DataDictionary()


def write_to_excel(all_data, output_file, app_name, use_section_name=False, workflow_template=None):
    def write_summary_sheets(wb, all_data):
        gadget_summary_df = formatter.get_gadget_summary(all_data)
        gadget_summary_df = formatter.convert_column_names_to_strings(
            gadget_summary_df)
        ws_gadget_summary = formatter.create_and_configure_worksheet(
            wb, 'Gadget Summary', {"A": 50})
        formatter.write_data_to_worksheet(ws_gadget_summary, gadget_summary_df)
        formatter.add_tables_and_styles(ws_gadget_summary, "GadgetSummary")

        gadget_summary_by_section = formatter.get_gadget_summary_by_section(
            all_data)
        gadget_summary_by_section = formatter.convert_column_names_to_strings(
            gadget_summary_by_section)
        ws_summary_by_section = formatter.create_and_configure_worksheet(
            wb, "Gadget Summary by Section", {"A": 75})
        formatter.write_data_to_worksheet(ws_summary_by_section,
                                          gadget_summary_by_section)
        formatter.add_tables_and_styles(
            ws_summary_by_section, "GadgetSummaryBySection")

        answerable_question_count = formatter.get_answerable_question_count(
            gadget_summary_by_section, formatter.gadgets_answerable)
        ws_answerable_count = formatter.create_and_configure_worksheet(
            wb, "Answerable Question Count", {"A": 25, "B": 10})
        ws_answerable_count.append(
            ["Answerable Question Count", answerable_question_count])

    def write_sql_template_sheet(wb, all_data, use_section_name, app_name):
        question_by_section = formatter.get_question_by_section(all_data)
        question_by_section = formatter.convert_column_names_to_strings(
            question_by_section)

        ws_sql_template = wb.create_sheet(title='SQL Helper')

        ws_sql_template.append(["Section", "Question Number", "Question",
                                "Form Key", "Field ID", "Data Type", "Gadget", "Custom json key", "Suggested key"])

        def remove_leading_digits(s):
            return re.sub('^[0-9]+', '', s).strip()

        def shorten_name(question_by_section, word_limit=4):
            name = re.sub('[^0-9a-zA-Z]+', ' ', question_by_section).lower()
            name = name.strip()
            name = re.sub(r'\d+$', '', name).strip()
            name = remove_leading_digits(question_by_section)
            words = word_tokenize(name)
            words = [word.lower() for word in words if len(word) >
                     1]  # disregard words of length 1
            stop_words = set(stopwords.words('english'))
            words = [word for word in words if word not in stop_words]
            freq_dist = FreqDist(words)
            common_words = [word for word, freq in freq_dist.most_common()]
            shortened_name = '_'.join(common_words[:word_limit])

            return shortened_name

        question_number = 1
        for index, row in question_by_section.iterrows():
            section = row["Section"]
            question = row["Question"]
            form_key = row["Form Key"]
            field_id = row["Field ID"]
            data_type = row["Data Type"]
            gadget = row["Gadget"]
            custom_json_key = row["Custom json key"]
            suggested_key = shorten_name(row["Question"])

            ws_sql_template.append([
                section,
                question_number,
                question,
                form_key,
                field_id,
                data_type,
                gadget,
                custom_json_key,
                suggested_key
            ])

            question_number += 1

        # Set column widths for sql helper tab
        for i, col in enumerate(ws_sql_template.columns, start=1):
            col_letter = get_column_letter(i)
            ws_sql_template.column_dimensions[col_letter].width = 25
        try:
            if use_section_name:
                section_name_count = {}
                for section_name in all_data.keys():
                    if section_name not in section_name_count:
                        section_name_count[section_name] = 1
                    else:
                        section_name_count[section_name] += 1

                    section_number = section_name_count[section_name]
                    section_alpha_mapping = chr(64 + (section_number % 26))
                    table_name = "TABLE_PLACEHOLDER"
                    break
            else:
                table_name = "Data"
        finally:
            table_name = "Data"

        formatter.add_tables_and_styles(ws_sql_template, "SQLTemplate")

    wb = Workbook()
    wb.remove(wb.active)

    section_number = 1
    tabbing = 0
    sql_tracking = 0

    section_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    invalid_chars = r'[\[\]:\*?/\\]'
    section_alpha_mapping = {}
    section_name_count = {}

    col_widths = {
        "A": 25, "B": 17, "C": 14, "D": 10, "E": 30,
        "F": 50, "G": 10, "H": 11, "I": 11, "J": 25,
        "K": 13, "L": 13, "M": 20, "N": 10, "O": 60, "P": 17, "Q": 100
    }

    for sheet_name, data in all_data.items():
        if isinstance(data, list):
            data = [item for item in data if isinstance(item, dict)]

        df = pd.DataFrame(data)

        df['Display logic'] = df['Display logic'].apply(
            lambda x: formatter.format_display_logic(x, all_data))
        df['Option key'] = df['Option key'].apply(formatter.numbered_list)
        df['Option label'] = df['Option label'].apply(formatter.numbered_list)

        if use_section_name:
            if not isinstance(sheet_name, str):
                sheet_name = str(sheet_name)
            sheet_name = re.sub(invalid_chars, " ", sheet_name)
            sheet_name = sheet_name.replace("\n", "")
            sheet_name = sheet_name[:30]

        else:
            if tabbing >= len(section_letters):
                tabbing = 0
            sheet_name = f"Section {section_letters[tabbing]}"
            tabbing += 1

        ws = formatter.create_and_configure_worksheet(
            wb, sheet_name, col_widths)
        formatter.write_data_to_worksheet(ws, df)

        section_alpha_mapping[sheet_name] = section_letters[sql_tracking % len(
            section_letters)]

        table_name = f"Section{section_alpha_mapping[sheet_name]}{section_number}"
        formatter.add_tables_and_styles(ws, table_name)
        section_number += 1
        sql_tracking += 1

    write_summary_sheets(wb, all_data)
    write_sql_template_sheet(wb, all_data, use_section_name, app_name)
    formatter.write_workflow_items_sheet(wb, workflow_template)

    wb.save(output_file)


def save_data_to_excel(data, filename):
    wb = Workbook()

    for sheet_name, records in data.items():
        if not records:
            continue

        ws = wb.active if not wb.active.title else wb.create_sheet(
            title=sheet_name)

        headers = list(records[0].keys())
        for col_num, header in enumerate(headers, 1):
            col_letter = get_column_letter(col_num)
            ws[f"{col_letter}1"] = str(header)
            ws.column_dimensions[col_letter].width = len(header) + 2

        for row_num, record in enumerate(records, 2):
            for col_num, value in enumerate(record.values(), 1):
                ws.cell(row=row_num, column=col_num, value=value)

    wb.save(output_file) if isinstance(
        output_file, str) else wb.save(output_file.name)


async def get_response_data(url, headers, data):
    async with httpx.AsyncClient(http2=True, timeout=60.0) as client:
        response = await client.post(url, headers=headers, json=data)
        response_json = response.json()
        jsos_str = json.dumps(response_json, indent=2)
        with open('json_file.json', 'w') as json_file:
            json.dump(response_json, json_file, indent=2, ensure_ascii=False)

    templates = {}
    app_name = jsearch("data.app.name", response_json)
    logger.info(app_name)
    iterations = None
    workflow_template = None
    children_template = None
    main_template = {}
    for f in range(40):
        section_type = jsearch(
            f"data.app.formContainer.template.children[{f}].type", response_json)
        section_label = jsearch(
            f"data.app.formContainer.template.children[{f}].label", response_json)
        for i in range(40):
            main_template = jsearch(
                JmespathQueries.main_template(f, i), response_json)
            children_template = jsearch(
                JmespathQueries.children_template(f, i), response_json)

            workflow_template = jsearch(
                JmespathQueries.workflow_template(), response_json)

            if main_template or children_template:
                if section_label not in templates:
                    templates[section_label] = []

            if main_template:
                if isinstance(main_template, dict) and main_template["Gadget"] not in ["Row", "Column"]:
                    templates[section_label].append(main_template)
                elif isinstance(main_template, list):
                    for item in main_template:
                        if isinstance(item, dict) and item["Gadget"] not in ["Row", "Column"]:
                            templates[section_label].append(item)

            if children_template:
                if isinstance(children_template, dict) and children_template["Gadget"] not in ["Row", "Column"]:
                    templates[section_label].append(children_template)
                elif isinstance(children_template, list):
                    for item in children_template:
                        if isinstance(item, dict) and item["Gadget"] not in ["Row", "Column"]:
                            templates[section_label].append(item)

        with open('templates.json', 'w') as json_file:
            json.dump(main_template, json_file, indent=2, ensure_ascii=False)

    return app_name, templates, workflow_template


async def main_dictionary(id: str):
    variables = {"id": id}
    data = {"query": query, "variables": variables}
    app_name, templates, workflow_template = await get_response_data(url, headers, data)
    for section_label, items in templates.items():
        items = [item for item in items if isinstance(item, dict)]
        for item in items:
            if 'Description' in item:
                item['Description'] = formatter.remove_html_tags(
                    item['Description'])
            else:
                item['Description'] = ''
    output = BytesIO()
    write_to_excel(templates, output, app_name,
                   use_section_name=True, workflow_template=workflow_template)
    output.seek(0)

    return output, f"{app_name}.xlsx"


if __name__ == "__main__":
    uvloop.install()
    asyncio.run(main_dictionary(id))
