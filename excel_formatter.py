# Path to file: /home/sjc98/kb_dictionary/excel_formatter.py

import sys
import os
import re
import math
import pandas as pd
from typing import Dict, List, Any, Tuple
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.styles import Alignment
from openpyxl.worksheet.table import Table, TableStyleInfo
import wordninja
import traceback

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

class ExcelFormatter:
    def __init__(self):
        self.gadgets_answerable = {
            'Text': 'Yes',
            'Radios': 'Yes',
            'Checkboxes': 'Yes',
            'Spacer': 'No',
            'Table': 'No',
            'Textarea': 'Yes',
            'Dropdown': 'Yes',
            'Signature': 'Yes',
            'Number': 'Yes',
            'Section': 'No',
            'DataLink': 'No',
            'FileUpload': 'Yes',
            'ReadOnlyData': 'No',
            'RichText': 'Yes',
            'IntegrationFill': 'Yes',
            'IntegrationTypeahead': 'Yes',
            'IntegrationMultiselect': 'Yes',
            'Date': 'Yes'
        }

        self.gadgets_to_tables = {
            'Text': 'Yes',
            'Radios': 'Yes',
            'Checkboxes': 'Yes',
            'Spacer': 'No',
            'Table': 'No',
            'Textarea': 'Yes',
            'Dropdown': 'Yes',
            'Signature': 'Yes',
            'Number': 'Yes',
            'Section': 'No',
            'DataLink': 'No',
            'FileUpload': 'Yes',
            'ReadOnlyData': 'No',
            'RichText': 'Yes',
            'IntegrationFill': 'Yes',
            'IntegrationTypeahead': 'Yes',
            'IntegrationMultiselect': 'Yes',
            'Date': 'Yes'
        }

        self.sql_characters = {
            'text': 'NVARCHAR(?)',
            'radios': 'NVARCHAR({})',
            'checkboxes': 'NVARCHAR({})',
            'textarea': 'NVARCHAR(?)',
            'dropdown': 'NVARCHAR({})',
            'number': 'INTEGER',
            'datalink': 'NVARCHAR(?)',
            'richtext': 'NVARCHAR(?)',
            'date': 'DATETIME'
        }


    def convert_column_names_to_strings(self, df):
        return df.rename(columns=lambda x: str(x))


    def get_answerable_question_count(self, summary_df, gadgets_answerable):
        answerable_counts = []

        for index, row in summary_df.iterrows():
            answerable_status = gadgets_answerable.get(row["Gadget"], "No")
            answerable_count = row["Count"] if answerable_status == "Yes" else 0
            answerable_counts.append(answerable_count)
        return sum(answerable_counts)


    def remove_html_tags(self, text):
        if not isinstance(text, str):
            return text
        return re.sub("<[^<]+?>", "", text)


    def get_option_label_by_key(self, option_key: str, option_labels: List[str], option_keys: List[str]) -> str:
        if option_key in option_keys:
            index = option_keys.index(option_key)
            return option_labels[index]
        return option_key


    def replace_form_key_and_value_with_question_and_option(self, form_key: str, value: str, templates: Dict[str, List[Dict[str, Any]]]) -> Tuple[str, str]:
        for section_label, items in templates.items():
            for item in items:
                if isinstance(item, dict) and 'Kuali json key' in item and item['Kuali json key'] == form_key:

                    question = item['Question']
                    if item['Option label'] is not None:
                        option_labels = item['Option label'].split("; ")
                        option_keys = item['Option key'].split("; ")
                        option_label = self.get_option_label_by_key(value, option_labels, option_keys)
                    else:
                        option_label = value
                    return question, option_label
        return form_key, value


    def replace_form_key_with_question(self, form_key, templates):
        for section_label, items in templates.items():
            for item in items:
                if isinstance(item, dict) and 'Kuali json key' in item and item['Kuali json key'] == form_key:
                    return item['Question']
        return form_key


    def format_display_logic(self, value, templates):
        if not value:
            return ""

        if isinstance(value, float):
            return str(value)
        formatted_values = []
        for i, item in enumerate(value):
            form_key = item.get("formKey", "").replace("data.", "")
            data_type = item.get("data", {}).get("type", "")
            data_value = item.get("data", {}).get("value", "")

            form_key, data_value = self.replace_form_key_and_value_with_question_and_option(
                form_key, data_value, templates)

            if data_value:
                formatted_values.append(
                    f"{i+1}. {form_key.strip()} = {data_value} ({' '.join(wordninja.split(data_type)).lower() if isinstance(data_type, str) else data_type})")
            else:
                formatted_values.append(
                    f"{i+1}. {form_key.strip()} {' '.join(wordninja.split(data_type)).lower() if isinstance(data_type, str) else data_type}")

        return "\n".join(formatted_values)


    def numbered_list(self, value: str) -> str:
        if value is None:
            return ""
        elif isinstance(value, float):
            return str(value)
        else:
            items = value.split('; ')
            return "\n".join(f"{i+1}. {item}" for i, item in enumerate(items))


    def get_gadget_summary(self, all_data):
        gadgets = []
        for data in all_data.values():
            if isinstance(data, list):
                data = [item for item in data if isinstance(item, dict)]
                gadgets.extend([item.get(
                    'Gadget') for item in data if 'Gadget' in item and item['Gadget'] is not None])

        gadget_counts = pd.Series(gadgets, dtype='object').value_counts()
        gadget_counts_df = gadget_counts.reset_index()
        gadget_counts_df.columns = ['Gadget', 'Count']
        return gadget_counts_df


    def get_gadget_summary_by_section(self, all_data):
        summary = []

        for section_name, data in all_data.items():
            if isinstance(data, list):
                data = [item for item in data if isinstance(item, dict)]
                gadgets = [item.get('Gadget')
                        for item in data if item.get('Gadget') is not None]

                gadget_counts = pd.Series(gadgets).value_counts()
                for gadget, count in gadget_counts.items():
                    summary.append(
                        {"Section": section_name, "Gadget": gadget, "Count": count})

        summary_df = pd.DataFrame(summary)
        return summary_df


    def get_question_by_section(self, all_data):
        summary = []

        for section_name, data in all_data.items():
            if isinstance(data, list):
                data = [item for item in data if isinstance(item, dict)]

                for item in data:
                    if self.gadgets_to_tables.get(item.get("Gadget")) == "Yes":
                        max_option_label_length = "?"

                        # Search for the item in the original data
                        for section_data in all_data.values():
                            if isinstance(section_data, list):
                                for original_item in section_data:
                                    if original_item.get('Kuali json key') == item['Kuali json key']:
                                        if 'Option label' in original_item and isinstance(original_item['Option label'], str):
                                            option_labels = original_item['Option label'].split(
                                                "; ")
                                            max_option_label_length = max(
                                                len(label) for label in option_labels)
                                        break

                        # Get the appropriate VARCHAR type for the current gadget
                        gadget = item.get("Gadget")
                        sql_type = self.sql_characters.get(
                            gadget.lower(), "VARCHAR({})")

                        # Format the VARCHAR type with the max_option_label_length value
                        if "{}" in sql_type:
                            data_type = sql_type.format(max_option_label_length)
                        else:
                            data_type = sql_type

                        summary.append({
                            "Section": section_name,
                            "Question": item["Question"],
                            "Form Key": item["Kuali json key"],
                            "Field ID": item["Field ID"],
                            "Data Type": data_type,
                            "Gadget": gadget,
                            "Custom json key": item.get("Custom json key"),
                            "Max Option Label Length": max_option_label_length
                        })

        summary_df = pd.DataFrame(summary)
        return summary_df


    def autofit_rows(self, ws):
        for row in ws.iter_rows():
            max_row_height = 15
            for cell in row:
                cell_height = 15
                if cell.value:
                    cell_width = ws.column_dimensions[cell.column_letter].width
                    wrapped_text_width = len(str(cell.value)) * 1.2
                    required_height = math.ceil(
                        wrapped_text_width / cell_width) * 15
                    cell_height = max(cell_height, required_height)

                max_row_height = max(max_row_height, cell_height)

            ws.row_dimensions[row[0].row].height = max_row_height



    def create_and_configure_worksheet(self, wb, sheet_name, col_widths):
        ws = wb.create_sheet(title=sheet_name)
        for col, width in col_widths.items():
            ws.column_dimensions[col].width = width
        return ws


    def write_data_to_worksheet(self, ws, df):
        for r in dataframe_to_rows(df, index=False, header=True):
            ws.append(r)
        self.autofit_rows(ws)
        for row in ws.iter_rows():
            for cell in row:
                cell.alignment = Alignment(
                    horizontal='left', vertical='top', wrap_text=True)


    def write_workflow_items_sheet(self, wb, workflow_template):
        if workflow_template is None:
            return

        ws_workflow_items = self.create_and_configure_worksheet(wb, "Workflow Items", {
            "A": 20, "B": 20, "C": 20, "D": 20, "E": 20,
            "F": 25, "G": 20, "H": 20, "I": 25, "J": 20,
            "K": 20, "L": 20, "M": 20, "N": 20, "O": 20,
            "Q": 20, "R": 20, "S": 20, "T": 20, "U": 20,
            "V": 20, "W": 20, "X": 20, "Y": 20, "Z": 20,
            "AA": 20,
        })

        workflow_template_df = pd.DataFrame(workflow_template)
        self.write_data_to_worksheet(ws_workflow_items, workflow_template_df)
        self.add_tables_and_styles(ws_workflow_items, "WorkflowItems")


    def add_tables_and_styles(self, ws, table_name):
        first_cell = ws.cell(row=1, column=1)
        last_cell = ws.cell(row=ws.max_row, column=ws.max_column)

        table_range = f"{first_cell.coordinate}:{last_cell.coordinate}"
        table = Table(displayName=table_name, ref=table_range)

        style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False,
                            showLastColumn=False, showRowStripes=True, showColumnStripes=False)
        table.tableStyleInfo = style
        ws.add_table(table)
