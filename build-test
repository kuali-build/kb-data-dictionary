#!/bin/sh
set -e

# Variables
PROJECT_NAME="$CI_PROJECT_NAME"
OWNER_GROUP="jovyan:jovyan"
CLEANUP_DAYS=30
MAX_PREVIOUS=1

hostname=$(hostname)
domain=$(hostname -d)
fqdn="${hostname}.${domain}"
CURRENT_DIR=$(pwd)
TIMESTAMP=$(date +"%Y-%m-%d-%H-%M-%S")
NEW_DIR="/srv/persistent-data/app/$PROJECT_NAME/new/$TIMESTAMP"
OLD_DIR="/srv/persistent-data/app/$PROJECT_NAME/$PROJECT_NAME"
PREVIOUS_DIR="/srv/persistent-data/app/$PROJECT_NAME/previous/$PROJECT_NAME-$TIMESTAMP"

# Functions
error_handling() {
    echo "An error occurred. Exiting..." | tee -a deploy.log
    exit 1
}

cleanup() {
    echo "Cleaning up old directories..."
    previous_count=$(find /srv/persistent-data/app/$PROJECT_NAME/previous/* -type d -maxdepth 0 | wc -l)
    if [ "$previous_count" -gt "$MAX_PREVIOUS" ]; then
        directories_to_remove=$((previous_count - MAX_PREVIOUS))
        echo "Removing $directories_to_remove oldest directories"
        find /srv/persistent-data/app/$PROJECT_NAME/previous/* -type d -mtime +$CLEANUP_DAYS -printf '%T@ %p\n' | sort -n | head -n "$directories_to_remove" | awk '{print $2}' | xargs rm -rf
    else
        echo "No need to remove old directories"
    fi
}

# Main script
trap error_handling ERR

# Ensure directories exist
mkdir -p "/srv/persistent-data/app/$PROJECT_NAME/new"
mkdir -p "/srv/persistent-data/app/$PROJECT_NAME/$PROJECT_NAME"
mkdir -p "/srv/persistent-data/app/$PROJECT_NAME/previous"

mkdir -p "$NEW_DIR"
chown -R gitlab-runner "$NEW_DIR"
cp -R . "$NEW_DIR"

git -C "$NEW_DIR" config --global --add safe.directory "$NEW_DIR"

git config --global --add safe.directory "$NEW_DIR"

cd "$NEW_DIR"
echo "$TIMESTAMP" > build-timestamp
echo "Working Directory: $(pwd)"
ls -ltr

COMMIT_HASH=$(git rev-parse HEAD)
echo "Commit Hash: $COMMIT_HASH"

# Build Docker image with no cache
docker-compose build --no-cache --build-arg COMMIT_HASH="$COMMIT_HASH"

if [ -d "$OLD_DIR" ]; then
  mv "$OLD_DIR" "$PREVIOUS_DIR"
fi

mv "$NEW_DIR" "$OLD_DIR"

chown -R $OWNER_GROUP "$OLD_DIR"

cd "$OLD_DIR"
docker-compose up -d

# Testing
SERVICE_STATUS=$(docker-compose ps web | grep Up)
if [ -z "$SERVICE_STATUS" ]; then
    echo "Service is not running. Exiting..."
    exit 1
fi

cleanup

# Prune all unused images, not just dangling ones
docker system prune --all --force

echo "Deployment of $PROJECT_NAME was successful. This thing should now be running as advertised."
