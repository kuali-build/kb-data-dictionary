# Path to file: /home/sjc98/kb_dictionary/list_apps.py

# Standard libraries
import sys
import os
import json
from enum import Enum, auto
from json import JSONDecodeError
from loguru import logger

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

def generate_app_enum():
    file_name = 'apps.json'

    script_dir = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(script_dir, file_name)

    try:
        with open(file_path, 'r') as f:
            apps = json.load(f)
    except JSONDecodeError:
        logger.warning("Error: apps.json is empty or contains invalid JSON data. Creating an empty AppEnum.")
        apps = []

    app_dict = {app["id"]: app["name"] for app in apps}
    app_name_dict = {app["id"]: app["id"] for app in apps}
    app_name_to_id = app_dict
    KualiAppsEnum = Enum('KualiAppsEnum', app_name_dict)
    return KualiAppsEnum, app_name_to_id


KualiAppsEnum, app_name_to_id = generate_app_enum()
